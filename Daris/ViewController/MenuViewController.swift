//
//  MenuViewController.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 9/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {

    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var notificationNumberLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var gradeLabel: UILabel!
    
    var usermodel:UserModel?
    
    var curriculamNotiCount:Int = 0
    var twasolNotiCount:Int = 0
    var scheduleNotiCount:Int = 0
    var homeworkNotiCount:Int = 0
    var examNotiCount:Int = 0
    var classNotiCount:Int = 0
    var certificateNotiCount:Int = 0
    var tabaniNotiCount:Int = 0
    
    var menuArray = [Dictionary<String,Any>]()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        
        APIManager.manager.newNotiCountModule(){(status, msg, notiCountDic) in
            
            
            self.curriculamNotiCount = notiCountDic!["crlm"] as! Int
            self.twasolNotiCount = notiCountDic!["tw"] as! Int
            
            print("self.twasolNotiCount ",self.twasolNotiCount)
            //add menu item with title and Image and background color
            self.addMenuItem()
            self.menuCollectionView.reloadData()
        }
        
 //       print("user model in MenuVC",usermodel!.user_name!)
    
    //    populateUserInfo()

    }
    
    func populateUserInfo() {
        
//        nameLabel.text = usermodel!.user_name
//        gradeLabel.text = "Grade \(usermodel!.level_id!) Class \(usermodel!.classroom_id!)"
//        let urlStr = "\(API_K.BaseURL)\(usermodel!.avatar ?? "")"
//        print("image url",urlStr)
//        self.profilePic?.kf.setImage(with: urlStr.asImageResource(),placeholder: UIImage.init(named: "demo-pic"), options: nil, progressBlock: nil, completionHandler: nil)
    }
    
    func addMenuItem() {
        
       // UIColor("#384D78")

        
        menuArray.append(["title":"Twasol", "icon":"menu-twasol","color":"#43BEA7","count":twasolNotiCount])
        menuArray.append(["title":"Schedule", "icon":"menu-schedule","color":"#A5CC2A","count":scheduleNotiCount])
        menuArray.append(["title":"Homework", "icon":"menu-homework","color":"#F18019","count":homeworkNotiCount])
        menuArray.append(["title":"Exam", "icon":"menu-exam","color":"#4D1A7F","count":examNotiCount])
        menuArray.append(["title":"Certificates", "icon":"menu-certificate","color":"#139BEB","count":certificateNotiCount])
        menuArray.append(["title":"Tabani", "icon":"menu-tabani","color":"#E40052","count":tabaniNotiCount])
        menuArray.append(["title":"Curriculum", "icon":"menu-curriculum","color":"#31A739","count":curriculamNotiCount])
        menuArray.append(["title":"Interactive Classroom", "icon":"menu-classroom","color":"#8A0B51","count":classNotiCount])
        
       
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return menuArray.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath ) as! MenuCollectionViewCell
        
        
        cell.menuTitle.text = menuArray[indexPath.item]["title"]! as? String
        cell.menuIcon.image = UIImage(named: menuArray[indexPath.item]["icon"]! as! String)
        
        let colorString = menuArray[indexPath.item]["color"]! as! String
        
        cell.backgroundColor = UIColor(colorString)
        
        let notiCount = menuArray[indexPath.item]["count"]! as! Int
        
        print("noti count ????:",notiCount)
        
        if  notiCount != 0 {
            
            print("noti count :",notiCount)
            
            cell.notiCountLabel.isHidden = false
            cell.notiCountLabel.text = "\(notiCount)"
        }
        
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let padding: CGFloat =  15
        let collectionViewSize = collectionView.frame.size.width - padding
        
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2 + 5)
    }
    
    
    @IBAction func homeButtonAction(_ sender: Any) {
        
        
    }
    
    
    @IBAction func settingButtonAction(_ sender: Any) {
        
        
    }
    

}
