//
//  LogInViewController.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 8/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import SVProgressHUD

class LogInViewController: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    
    var userinfo:UserModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        passWordTextField.delegate = self
        userNameTextField.delegate = self
        
        
        
    }
    
    // To hide keyboard when touch on the view (outside the UITextField)
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        userNameTextField.resignFirstResponder()
        passWordTextField.resignFirstResponder()
        
    }
    
    
    

    @IBAction func logInButtonAction(_ sender: Any) {
        
        
        let vmsg = ValidationManager.manager.validateLoginForm(userName: userNameTextField.text!, password: passWordTextField.text!)
        if !vmsg.isEmpty{
            showConfimationAlert(vmsg)
            return
        }
        APIManager.manager.login(userName: userNameTextField.text!, password: passWordTextField.text!) { (status, token, msg, userInfo) in
            if status{
                SVProgressHUD.showSuccess(withStatus: msg)
                AppSessionManager.shared.authToken = token
                AppSessionManager.shared.save()
                
                self.userinfo = userInfo!
                
                print("self.userinfo in login vc :",self.userinfo!.user_name!)
                
                Routes.setDrawerAsRootViewController(with: self.userinfo!)
                
            }
            else{
                SVProgressHUD.showError(withStatus: msg)
            }
        }

        

    }
    
    @IBAction func rememberMeButtonAction(_ sender: Any) {
        
        
    }
    
    
    @IBAction func reSetPassButtonAction(_ sender: Any) {
        
        Routes.gotoForgetPassView(from: self)
        
    }
}
