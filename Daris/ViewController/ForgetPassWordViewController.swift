//
//  ForgetPassWordViewController.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 27/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class ForgetPassWordViewController: UIViewController {
    
    
    @IBOutlet weak var backGroundImageView: UIImageView!
    
    @IBOutlet weak var userNameView: UIView!
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var codeView: UIView!
    
    @IBOutlet weak var mobileNoTextField: UITextField!
    
    @IBOutlet weak var otpView: VPMOTPView!
    
    var enteredOtp: String = ""
    
    
    @IBOutlet weak var confirmPassView: UIView!
    
    @IBOutlet weak var newPasswordTextField: UITextField!
    
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var finalView: UIView!
    
    var userName:String?
    var phone:String?
    var code:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        setUpOtpView()
        
        
    }
    
    
    func setUpOtpView() {
        
        //OTP View
        otpView.otpFieldsCount = 6
        otpView.otpFieldDefaultBorderColor = UIColor.lightGray
        otpView.otpFieldEnteredBorderColor = UIColor.green
        //otpView.otpFieldErrorBorderColor = UIColor.red
        otpView.otpFieldBorderWidth = 1
        otpView.delegate = self
        otpView.shouldAllowIntermediateEditing = false
        
        let screenRect:CGRect = UIScreen.main.applicationFrame
        if (screenRect.size.height <= 568)
        {
            
            otpView.otpFieldSeparatorSpace = 8
        }
        
        // Create the UI
        otpView.initializeUI()

    }

    @IBAction func nextButtonAction(_ sender: Any) {
        
        
        let vmsg = ValidationManager.manager.validateUserName(userName: userNameTextField.text!)
        if !vmsg.isEmpty{
            showConfimationAlert(vmsg)
            return
        }
        
        userName = userNameTextField.text
        
        
        
        APIManager.manager.resetPasswordRequest(userName: userName!) { (status, msg) in

            if status{

                self.userNameView.isHidden = true
                self.codeView.isHidden = false
            }

        }
        
        
    }
    
    @IBAction func nextButtonActionInCodeView(_ sender: Any) {
        
        if !(mobileNoTextField.text?.isEmpty)! && enteredOtp.count == 6 {
            
            phone = mobileNoTextField.text
            code = enteredOtp
            
            APIManager.manager.resetCode(userName: userName!, phone: phone!, code: code!) { (status, msg) in
                
                if status{
                    
                    self.userNameView.isHidden = true
                    self.codeView.isHidden = true
                    self.confirmPassView.isHidden = false
                }
                
            }
            
            
        }else{
            
            
        }
        
        
        
    }
    
    @IBAction func nextButtonActionInConfirmPassView(_ sender: Any) {
        
        if  !(newPasswordTextField.text?.isEmpty)! && !(confirmPasswordTextField.text?.isEmpty)!{
            
            if newPasswordTextField.text == confirmPasswordTextField.text {
                
                APIManager.manager.resetPassWord(userName: userName!, phone: phone!, code: code!, password: newPasswordTextField.text!, password_confirmation: confirmPasswordTextField.text!) { (status, msg) in
                    
                    
                    if status {
                        
                        self.userNameView.isHidden = true
                        self.codeView.isHidden = true
                        self.confirmPassView.isHidden = true
                        self.finalView.isHidden = false
                    }
                }
            }else{
                
                
            }
        }else{
            
            
        }
        
    }
    
    @IBAction func goToLogInButtonAction(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}

extension ForgetPassWordViewController: VPMOTPViewDelegate {
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        
        return enteredOtp == "123456"
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return false
    }
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        print("OTPString: \(otpString)")
    }
}
