//
//  HomeViewController.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 8/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit



class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var classRoutineTableView: UITableView!
    
    @IBOutlet weak var notificationTableView: UITableView!
    
    @IBOutlet weak var notificationView: UIView!
    
    @IBOutlet weak var notificationNumberLabel: UILabel!
    
    @IBOutlet weak var homeworkNumberLabel: UILabel!
    
    @IBOutlet weak var homeworkProgressView: UIProgressView!
    
    @IBOutlet weak var classroomNumberLabel: UILabel!
    
    @IBOutlet weak var classroomProgressView: UIProgressView!
    
    @IBOutlet weak var examNumberLabel: UILabel!
    
    @IBOutlet weak var examProgressView: UIProgressView!
    
    @IBOutlet weak var tabaniNumberLabel: UILabel!
    
    @IBOutlet weak var tabaniProgressView: UIProgressView!
    
    @IBOutlet weak var noNotificationLabel: UILabel!
    var notifications : [NotificationModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        classRoutineTableView.delegate = self
        classRoutineTableView.dataSource = self
        classRoutineTableView.tableFooterView = UIView()
        
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
        notificationTableView.tableFooterView = UIView()
        
       
        APIManager.manager.notiReadAll()
        APIManager.manager.notiReadSingle()
        APIManager.manager.notiReadModuleAll()
        
        
        
        APIManager.manager.newNotiCount(){(status, msg, notiCount) in
            
            print("notiCount : ",notiCount!)
            
            if notiCount != 0{
                
                self.notificationNumberLabel.isHidden = false
                self.notificationNumberLabel.text = "\(notiCount ?? 0 )"
                
            }
        }
        
        APIManager.manager.notificationGetAll(){ (notifications) in
            
            print("notifications in home",notifications)
            self.notifications = notifications
            if self.notifications.count == 0{
                
                self.notificationTableView.isHidden = true
                self.noNotificationLabel.isHidden = false
                
            }else{
                
                self.notificationTableView.isHidden = false
                self.noNotificationLabel.isHidden = true
                self.notificationTableView.reloadData()
            }
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if tableView == classRoutineTableView {
          
            return 1
            
        }else{
            //
            return notifications.count
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == classRoutineTableView {
            
            return 6
            
        }else{
            //
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == classRoutineTableView {
            
        
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "routineCell")!
        
             return cell
        }else{
            
            let cell  = tableView.dequeueReusableCell(withIdentifier: "notificationCell")! as! NotificationTableViewCell
            
            let singleNoti = self.notifications[indexPath.section]
            
            cell.setNotificationInfo(singleNoti)
            
            return cell

        }
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        

    }
    
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if tableView == notificationTableView {
            
            let cell : UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "headerCell")!
            
            return cell
        }
        
        return nil
        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        if tableView == notificationTableView {
//            
//            return 5
//        }
//        return 0
//    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if tableView == notificationTableView {
            
            return 5
        }
        return 0
    }
    
    
    @IBAction func menuButtonAction(_ sender: Any) {
        
        if let drawerController = navigationController?.parent as? KYDrawerController {
            
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    
    @IBAction func notificationButtonAction(_ sender: Any) {
        
        drawTriangle(size: 12, x: self.view.frame.size.width - 110 , y: -12, up: true)
        notificationView.isHidden = !notificationView.isHidden
    }
    
    
    
    func drawTriangle(size: CGFloat, x: CGFloat, y: CGFloat, up:Bool) {
        
        let colorstr = "#5C2D89"
        
        let triangleLayer = CAShapeLayer()
        let trianglePath = UIBezierPath()
        trianglePath.move(to: .zero)
        trianglePath.addLine(to: CGPoint(x: -size, y: up ? size : -size))
        trianglePath.addLine(to: CGPoint(x: size, y: up ? size : -size))
        trianglePath.close()
        triangleLayer.path = trianglePath.cgPath
        triangleLayer.fillColor = UIColor(colorstr).cgColor
        triangleLayer.anchorPoint = .zero
        triangleLayer.position = CGPoint(x: x, y: y)
        triangleLayer.name = "triangle"
        notificationView.layer.addSublayer(triangleLayer)
    }
    
    

}
