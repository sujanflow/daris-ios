//
//  UserModel.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 27/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import Gloss
import SwiftyJSON

class UserModel: Glossy {
    
    var user_id: Int?
    var user_name: String?
    var school_id: Int?
    var level_id: Int?
    var classroom_id:Int?
    var guardian_id:Int?
    var name:String?
    var phone: String?
    var email: String?
    var user_type: Int?
    var avatar: String?
    
    required init?(json: Gloss.JSON) {
        user_id = "id" <~~ json
        user_name = "username" <~~ json
        school_id = "school_id" <~~ json
        level_id = "level_id" <~~ json
        classroom_id = "classroom_id" <~~ json
        guardian_id = "guardian_id" <~~ json
        name = "name" <~~ json
        phone = "phone" <~~ json
        email = "email" <~~ json
        user_type = "user_type" <~~ json
        avatar = "avatar" <~~ json
        
    }
    
    func toJSON() -> Gloss.JSON? {
        return jsonify([
            "id" ~~> user_id,
            "user_name" ~~> user_name,
            "school_id" ~~> school_id,
            "level_id" ~~> level_id,
            "classroom_id" ~~> classroom_id,
            "guardian_id" ~~> guardian_id,
            "phone" ~~> phone,
            "email" ~~> email,
            "user_type" ~~> user_type,
            "name" ~~> name,
            "avatar" ~~> avatar
            
            ])
    }
}
