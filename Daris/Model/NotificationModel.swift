//
//  NotificationModel.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 3/12/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import Gloss
import SwiftyJSON

class NotificationModel: Glossy {
    
    var id: Int?
    var user_id: Int?
    var content: Dictionary<String, Any>?
    var module: Int?
    var module_id: Int?
    var is_active:Int?
    var created_at:String?
    var updated_at:String?
    var receiver_id: Int?
    var pn_type: String?
    var created_at_human: String?
    var sender:UserModel?
    var receiver:UserModel?
    
    required init?(json: Gloss.JSON) {
        id = "id" <~~ json
        user_id = "user_id" <~~ json
        content = "content" <~~ json
        module = "module" <~~ json
        module_id = "module_id" <~~ json
        is_active = "is_active" <~~ json
        created_at = "created_at" <~~ json
        updated_at = "updated_at" <~~ json
        receiver_id = "receiver_id" <~~ json
        pn_type = "pn_type" <~~ json
        created_at_human = "created_at_human" <~~ json
        sender = "sender" <~~ json
        receiver = "receiver" <~~ json
        
    }
    
    func toJSON() -> Gloss.JSON? {
        return jsonify([
            "id" ~~> id,
            "user_id" ~~> user_id,
            "content" ~~> content,
            "module" ~~> module,
            "module_id" ~~> module_id,
            "is_active" ~~> is_active,
            "created_at" ~~> created_at,
            "updated_at" ~~> updated_at,
            "receiver_id" ~~> receiver_id,
            "pn_type" ~~> pn_type,
            "created_at_human" ~~> created_at_human,
            "sender" ~~> sender,
            "receiver" ~~> receiver
            
            ])
    }

    

}
