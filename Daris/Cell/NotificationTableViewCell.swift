//
//  NotificationTableViewCell.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 8/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    @IBOutlet weak var moduleImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setNotificationInfo(_ notification:NotificationModel){
        
        print("setNotificationInfo.........")
        
        nameLabel.text = notification.sender?.user_name
        detailLabel.text = notification.pn_type
        
        let urlStr = "\(API_K.BaseURL)\(notification.sender?.avatar ?? "")"
        print("image url",urlStr)
        self.profilePic?.kf.setImage(with: urlStr.asImageResource(),placeholder: UIImage.init(named: "demo-pic"), options: nil, progressBlock: nil, completionHandler: nil)
        
        var img: UIImage?
        
        if notification.module == 1{
            
            img = UIImage(named: "NOTIFICATIONS-message")!
            
        }else if(notification.module == 2){
            
           img = UIImage(named: "NOTIFICATIONS-homework")!
            
        }else if(notification.module == 3){
            
            img = UIImage(named: "menu-curriculum")!
            
        }else if(notification.module == 4){
            
            img = UIImage(named: "NOTIFICATIONS-homework")!
            
        }else if(notification.module == 5){
            
            img = UIImage(named: "NOTIFICATIONS-homework")!
            
        }else if(notification.module == 6){
            
            img = UIImage(named: "NOTIFICATIONS-homework")!
            
        }else if(notification.module == 7){
            
            img = UIImage(named: "NOTIFICATIONS-homework")!
            
        }else if(notification.module == 8){
            
            img = UIImage(named: "NOTIFICATIONS-homework")!
        }
        
        moduleImage.image = img

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   

}
