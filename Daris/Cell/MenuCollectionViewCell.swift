//
//  MenuCollectionViewCell.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 9/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit

class MenuCollectionViewCell: UICollectionViewCell {
    
    
    
    @IBOutlet weak var menuIcon: UIImageView!
    
    @IBOutlet weak var menuTitle: UILabel!
    
    
    @IBOutlet weak var notiCountLabel: UILabel!
    
    
}
