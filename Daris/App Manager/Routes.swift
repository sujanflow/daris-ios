//
//  Routes.swift
//  Daris
//
//  Created by Md.Ballal Hossen on 11/11/18.
//  Copyright © 2018 Sujan. All rights reserved.
//

import UIKit
import Foundation

class Routes {
    
    static let mainSB = "Main"
    static let launchScreenSB = "LaunchScreen"
    static let authentication = "Authentication"
    
    init() {
        
    }

    class func setDrawerAsRootViewController(with usermodel:UserModel) {

        print("setDrawerAsRootViewController" , usermodel.user_name!)
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let homeStoryBoard = UIStoryboard(name: mainSB, bundle: nil)
        
        var drawerVC = homeStoryBoard.instantiateInitialViewController() as! KYDrawerController
       // print("test vc", drawerVC.mainViewController)
        drawerVC.userModel = usermodel 
        
        appDelegate?.window?.rootViewController = drawerVC
        appDelegate?.window?.makeKeyAndVisible()
        
        
    }
    
    class func gotoForgetPassView(from vc: UIViewController) {
        
        let forgetVC = UIStoryboard(name: self.authentication,
                                    bundle: nil).instantiateViewController(withIdentifier: "ForgetPassWordViewController") as! ForgetPassWordViewController
        vc.navigationController?.pushViewController(forgetVC, animated: true)
    }

}
