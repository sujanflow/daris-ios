//
//  APIManager
//  TT
//
//  Created by Dulal Hossain on 4/2/17.
//  Copyright © 2017 DL. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Gloss
import SVProgressHUD

//

extension String{
    func isTrue() -> Bool {
        return self == "True"
    }
}

enum RequestActionType:String {
    case accept = "add"
    case reject = "cancel"
    case remove = "remove"
}
enum RequestAction:String {
    case accept = "accepted"
    case reject = "rejected"
}

struct API_K {
    static let DEVICE_TYPE = "Android"
    static let DEV_KEY = "_ia"
    static let BaseUrlStr:String = "https://www.daaris.com/"
//https://www.daaris.com/en/api/v0.1/student/auth/login
    static let BaseURL = URL(string:"\(BaseUrlStr)en/api/v0.1/")!
    
    static let  LOGIN = "student/auth/login"
    static let  REGISTER = "registration"
    //https://www.daaris.com/en/api/v0.1/notification/get/count/new
    static let NEW_NOTI_COUNT = "notification/get/count/new"
    //https://www.daaris.com/en/api/v0.1/notification/get/module/count/new
    static let NEW_NOTI_COUNT_MODULE = "notification/get/module/count/new"
    //https://www.daaris.com/en/api/v0.1/notification/get/all
    static let NOTI_GET_ALL = "notification/get/all"
    //https://www.daaris.com/en/api/v0.1/notification/read/all
    static let NOTI_READ_ALL = "notification/read/all"
    //https://www.daaris.com/en/api/v0.1/notification/read/single
    static let NOTI_READ_SINGLE = "notification/read/single"
    //https://www.daaris.com/en/api/v0.1/notification/read/module/all
    static let NOTI_READ_MODULE_ALL = "notification/read/module/all"

    //https://www.daaris.com/en/api/v0.1/student/auth/reset/request
    static let RESET_REQUEST = "student/auth/reset/request"
    //https://daaris.com/en/api/v0.1/student/auth/reset/request/valid
    static let RESET_CODE = "student/auth/reset/request/valid"
    //https://daaris.com/en/api/v0.1/student/auth/reset/password
    static let RESET_PASSWORD = "student/auth/reset/password"
    
    
    static let  GET_COUNTRIES = "getCountries"
    static let  CREATE_OTP = "createOtp"
    static let  GET_PASSWORD = "retrievePassword"
    static let  logout = "logout"

    static let  ADD_FRIEND = "addFriend"
    static let  GET_FRIEND = "getFriend"
    static let GET_WOULD_FRIEND = "getWouldBeFriend"
    static let ACCEPT_REJECT_FRIEND = "actionToFriendRequest"
    static let REMOVE_FRIEND = "removeFriend"
}

struct API_STRING {
    
    static let  PROPILE_REVIEW_ALERT = "Please set up your profile"
    
    static let  LOGIN_VALIDATION_TEXT = "The duplicate key value is"
    
    static let  NOTE_ADD_SUCCESS = "Note sent successfully"
    static let  COMMENT_ADD_SUCCESS = "Feedback sent successfully"
    static let  POST_ADD_SUCCESS = "Upload ad successful"
    static let  POST_ADD_FAIL = "Upload ad failed"

    static let  POST_EDIT_SUCCESS = "Post edited successfully"
    static let  POST_EDIT_FAIL = "Post edit failed"

    static let  NOTE_ADD_FAIL = "Note sending failed"
    static let  COMMENT_ADD_FAIL = "Feedback sending failed"
    
    static let  DELETE_ADD_SUCCESS = "Ad delete successful"
    static let  DELETE_ADD_FAIL = "Ad delete failed"
}

struct APP_STRING {
   
    static let  SERVER_ERROR = "Something went wrong! Please, try later"
    static let PROGRESS_TEXT = "Please wait..."
    static let CommentPlaceHolder = "Write your comment"
    static let PostPlaceHolder = "Details"
    static let CategoryPlaceHolder = "Category"
    static let notePlaceHolder = "Write your note"
    static let EmptyDataText = "No ads are available"
}

enum ResponseType {
    case success
    case fail
    case invalid
}

class APIManager: NSObject {
    
    /*
     *-------------------------------------------------------
     * MARK:- singletone initialization
     *-------------------------------------------------------
     */
    
    private struct Static {
        static var intance: APIManager? = nil
    }
    
    private static var __once: () = {
        Static.intance = APIManager()
    }()
    
    class var manager: APIManager {
        _ = APIManager.__once
        return Static.intance!
    }
    
    func login(userName:String, password:String,withCompletionHandler completion:(( _ status: Bool,_ authToken:String?, _ message: String?,_ user:UserModel?)->Void)?) {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["username":userName,
                                      "password":password,
                                      "_dev":API_K.DEV_KEY
                                      ]
 
        Request(.post, API_K.LOGIN, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
              switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue

                    if isSuccess != 2000{
                        completion?(false,nil,msg,nil)
                    }
                    else{
                        let token:String = json["key"].stringValue

                      
                        if let userInfo = json["data"].dictionaryObject {
                            
                            if let user = UserModel.init(json: userInfo) {
                                
                              completion?(true,token,msg,user)
                                
                            }
                            
                        }
                    }
                }
                else {
                    completion?(false,nil,nil,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,nil,error.localizedDescription,nil)
            }
        })
    }
    
    
    func newNotiCount(withCompletionHandler completion:(( _ status: Bool,_ message: String?,_ notiCount: Int?) ->Void)?) {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
                                     ]

        Request(.post, API_K.NEW_NOTI_COUNT, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                   print("response ",jsonDic)
                    
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue

                    if isSuccess != 2000{
                        completion?(false,msg,0)
                    }
                    else{
                        
                        let count:Int = json["new"].intValue
                            
                            completion?(true,msg,count)
                        
                    }

                
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
    
    func newNotiCountModule(withCompletionHandler completion:(( _ status: Bool,_ message: String?,_ notiCountDic: Dictionary<String, Any>?) ->Void)?) {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
        ]
        
        Request(.post, API_K.NEW_NOTI_COUNT_MODULE, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    print("response ",jsonDic)
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue
                    
                    if isSuccess != 2000{
                        completion?(false,msg,nil)
                    }
                    else{
                        
                        if let countDic = json["new"].dictionaryObject{
                            
                             completion?(true,msg,countDic)
                            
                        }
                    }
                    
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
   
    func notificationGetAll(completion:(( _ notification: [NotificationModel])->Void)?) {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
        ]
        
        Request(.post, API_K.NOTI_GET_ALL, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    print("response ",jsonDic)
                    
                    
                    if let notificationArray = json["data"].arrayObject as? [Gloss.JSON] {
                        
                        if let singleNotification = [NotificationModel].from(jsonArray: notificationArray) {
                            
                            completion?(singleNotification)
                            
                        } else {
                            
                            completion?([])
                        }
                    }

                    
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
    func notiReadAll() -> Void {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
        ]
        
        Request(.post, API_K.NOTI_READ_ALL, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    print("response ",jsonDic)
                    
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
    func notiReadSingle() -> Void {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
        ]
        
        Request(.post, API_K.NOTI_READ_SINGLE, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    print("response ",jsonDic)
                    
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
    
    func notiReadModuleAll() -> Void {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["key":AppSessionManager.shared.authToken!,
                                      "_dev":API_K.DEV_KEY
        ]
        
        Request(.post, API_K.NOTI_READ_MODULE_ALL, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    print("response ",jsonDic)
                    
                }
                
            case .failure(let error):
                
                SVProgressHUD.dismiss()
                
            }
        })
        
    }
    
    func  resetPasswordRequest(userName:String,withCompletionHandler completion:(( _ status: Bool, _ message: String?)->Void)?){
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["username":userName]
        
        Request(.post, API_K.RESET_REQUEST, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue
                    
                    if isSuccess != 2000{
                        completion?(false,msg)
                    }
                    else{
                        
                        print("Success")
                        completion?(true,msg)
                    }
                }
                else {
                    completion?(false,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,error.localizedDescription)
            }
        })
        
    }
    
    func  resetCode(userName:String,phone:String,code:String,withCompletionHandler completion:(( _ status: Bool, _ message: String?)->Void)?){
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["username":userName,
                                      "phone":phone,
                                      "code":code]
        
        Request(.post, API_K.RESET_CODE, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue
                    
                    if isSuccess != 2000{
                        completion?(false,msg)
                    }
                    else{
                        
                        print("Success")
                        completion?(true,msg)
                    }
                }
                else {
                    completion?(false,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,error.localizedDescription)
            }
        })
        
    }
    
    func resetPassWord(userName:String,phone:String,code:String,password:String,password_confirmation:String,withCompletionHandler completion:(( _ status: Bool, _ message: String?)->Void)?){
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        let params:[String:String] = ["username":userName,
                                      "phone":phone,
                                      "code":code,
                                      "password":password,
                                      "password_confirmation":password_confirmation]
        
        Request(.post, API_K.RESET_PASSWORD, parameters: params)?.responseJSON(completionHandler: { (responseData) in
            
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                if let jsonDic = json.dictionaryObject {
                    
                    let isSuccess:Int = jsonDic["status"] as! Int
                    let msg:String = json["msg"].stringValue
                    
                    if isSuccess != 2000{
                        completion?(false,msg)
                    }
                    else{
                        
                        print("Success")
                        completion?(true,msg)
                    }
                }
                else {
                    completion?(false,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,error.localizedDescription)
            }
        })
        
    }
    
//    func logOut(completion:(( _ status: Bool, _ message: String?)->Void)?) {
//
//        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
//
//        Request(.post, API_K.logout, parameters: nil)?.responseJSON(completionHandler: { (responseData) in
//            switch responseData.result {
//            case .success(let value):
//                print(value)
//                SVProgressHUD.dismiss()
//
//                let json = JSON(value)
//                if let jsonDic = json.dictionaryObject {
//
//                    let isSuccess:Bool = jsonDic["success"] as! Bool
//                    let msg:String = json["message"].stringValue
//
//                    if !isSuccess{
//                        completion?(false,msg)
//                    }
//                    else{
//                        completion?(true,msg)
//                    }
//                }
//                else {
//                    completion?(false,nil)
//                }
//
//            case .failure(let error):
//                SVProgressHUD.dismiss()
//                completion?(false,nil)
//            }
//        })
//    }
//
//    func register(userName:String ,fullName:String, email:String,country:String,phone:String, password:String, withCompletionHandler completion:(( _ status: Bool,_ token:String?, _ message: String?)->Void)?) {
//
//        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
//
//        let params:[String:String] = ["userName":userName,
//                                      "password":password,
//                                      "pushToken":"1234567890",
//                                      "deviceType":"iphone",
//                                      "email":email,
//                                      "latitude":"23.90",
//                                      "longitude":"90.23",
//                                      "fullName":fullName,
//                                      "avatar":"",
//                                      "country":country,
//                                      "phone":phone]
//
//
//        Request(.post, API_K.REGISTER, parameters: params)?.responseJSON(completionHandler: { (responseData) in
//            switch responseData.result {
//            case .success(let value):
//                print(value)
//                SVProgressHUD.dismiss()
//                let json = JSON(value)
//                if let jsonDic = json.dictionaryObject {
//                    let isSuccess:Bool = jsonDic["success"] as! Bool
//                    let msg:String = json["message"].stringValue
//
//                    if !isSuccess{
//                        completion?(false,nil,msg)
//                    }
//                    else{
//                        let token:String = json["authToken"].stringValue
//                        completion?(true,token,msg)
//                    }
//                }
//                else {
//                    completion?(false,nil,nil)
//                }
//            case .failure(let error):
//                SVProgressHUD.dismiss()
//                completion?(false,nil,error.localizedDescription)
//            }
//        })
//    }
//
//    func getOtp(email:String, withCompletionHandler completion:(( _ status: Bool, _ message: String?)->Void)?) {
//
//        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
//
//        let params:[String:String] = ["email":email]
//
//        Request(.post, API_K.CREATE_OTP, parameters: params)?.responseJSON(completionHandler: { (responseData) in
//            switch responseData.result {
//            case .success(let value):
//                print(value)
//                SVProgressHUD.dismiss()
//                let json = JSON(value)
//                if let jsonDic = json.dictionaryObject {
//
//                    let isSuccess:Bool = jsonDic["success"] as! Bool
//                    let msg:String = json["message"].stringValue
//
//                    if !isSuccess{
//                        completion?(false,msg)
//                    }
//                    else{
//
//                        completion?(true,msg)
//                    }
//                }
//                else {
//                    completion?(false,nil)
//                }
//            case .failure(let error):
//                SVProgressHUD.dismiss()
//                completion?(false,error.localizedDescription)
//            }
//        })
//    }
//
//    func resetPassword(otp:String, password:String, withCompletionHandler completion:(( _ status: Bool, _ message: String?)->Void)?) {
//
//        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
//
//        let params:[String:String] = ["otp":otp,
//                                      "password":password,
//                                      "passwordConfirmation":password]
//
//        Request(.post, API_K.GET_PASSWORD, parameters: params)?.responseJSON(completionHandler: { (responseData) in
//            switch responseData.result {
//            case .success(let value):
//                print(value)
//                SVProgressHUD.dismiss()
//                let json = JSON(value)
//                if let jsonDic = json.dictionaryObject {
//
//                    let isSuccess:Bool = jsonDic["success"] as! Bool
//                    let msg:String = json["message"].stringValue
//
//                    if !isSuccess{
//                        completion?(false,msg)
//                    }
//                    else{
//
//                        completion?(true,msg)
//                    }
//                }
//                else {
//                    completion?(false,nil)
//                }
//            case .failure(let error):
//                SVProgressHUD.dismiss()
//                completion?(false,error.localizedDescription)
//            }
//        })
//    }
    


    

    func getDataList(_ param:[String:Any]? = nil,method:String, withCompletionHandler completion:(( _ status: Bool, _ data: [Gloss.JSON]?)->Void)?) {
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)

        Request(.post, method, parameters: param)?.responseJSON(completionHandler: { (responseData) in
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                
                if let jsonDic = json.dictionaryObject {
                    let isSuccess:Bool = jsonDic["success"] as! Bool
                    
                    if !isSuccess{
                        completion?(false,nil)
                    }
                    else{
                        if let locationsArray = json["data"]["lists"].arrayObject as? [Gloss.JSON] {
                            completion?(true,locationsArray)
                        }
                        else{
                            completion?(false,nil)
                        }
                    }
                }
                else {
                    completion?(false,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,nil)
            }
        })
    }
    
    func getDataModel(_ param:[String:Any]? = nil,method:String, withCompletionHandler completion:(( _ status: Bool, _ data: Gloss.JSON?,_ msg:String?)->Void)?) {
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        Request(.post, method, parameters: param)?.responseJSON(completionHandler: { (responseData) in
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                
                if let jsonDic = json.dictionaryObject {
                    let isSuccess:Bool = jsonDic["success"] as! Bool
                    let msg:String = json["message"].stringValue

                    if !isSuccess{
                        completion?(false,nil,msg)
                    }
                    else{
                        if let locationsArray = json["data"].dictionaryObject {
                            completion?(true,locationsArray,msg)
                        }
                        else{
                            completion?(false,nil,msg)
                        }
                    }
                }
                else {
                    completion?(false,nil,APP_STRING.SERVER_ERROR)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,nil,error.localizedDescription)
            }
        })
    }
    
    func getList(_ param:[String:Any]? = nil,method:String, withCompletionHandler completion:(( _ status: Bool, _ data: [Gloss.JSON]?)->Void)?) {
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        Request(.post, method, parameters: param)?.responseJSON(completionHandler: { (responseData) in
            switch responseData.result {
            case .success(let value):
                print(value)
                SVProgressHUD.dismiss()
                let json = JSON(value)
                
                if let jsonDic = json.dictionaryObject {
                    let isSuccess:Bool = jsonDic["success"] as! Bool
                    
                    if !isSuccess{
                        completion?(false,nil)
                    }
                    else{
                        if let locationsArray = json["data"].arrayObject as? [Gloss.JSON] {
                            completion?(true,locationsArray)
                        }
                        else{
                            completion?(false,nil)
                        }
                    }
                }
                else {
                    completion?(false,nil)
                }
            case .failure(let error):
                SVProgressHUD.dismiss()
                completion?(false,nil)
            }
        })
    }
    
    
}



struct API_K_PROFILE {
    static let MY_PROFILE = "myProfile"
    static let OTHER_PROFILE = "othersProfile"
    static let UPLOAD_PROFILE_PICTURE = "uploadProfilePicture"
    static let UPLOAD_COVER_PHOTO = "uploadCoverPhoto"
    static let UPDATE_PROFILE = "updateProfile"
}
extension APIManager{
    func getMyProfile(completion:(( _ status: Bool,_ user:UserModel?, _ message: String?)->Void)?) {
        
        getDataModel(method: API_K_PROFILE.MY_PROFILE) { (sts, dataModel,msg) in
            if sts{
                if let jsA = dataModel{
                    if let histories = UserModel.init(json: jsA) {
                        completion?(true,histories,msg)
                        
                    } else {
                        completion?(false,nil,msg)
                    }
                }
                else{
                    completion?(false,nil,msg)
                }
            }
            else{
                completion?(false,nil,msg)
            }
        }
    }

    
}





struct API_K_BLOG {
    static let GET_BLOG_LIST = "getBlogs"
    static let CREATE_BLOG = "createBlog"
    static let UPLOAD_BLOG_IMAGE = "uploadBlockImage"
    static let GET_BLOG_DETAILS = "othersProfile"
    static let UPLOAD_BLOG_VIDEO = "uploadBlogVideo"

}

extension APIManager{
    
    func uploadBlogImage(_ image:UIImage?, withCompletionHandler completion:(( _ status:Bool,_ url:String?, _ msg:String?)->Void)?) {
        
        SVProgressHUD.show(withStatus: APP_STRING.PROGRESS_TEXT)
        
        UploadRequest(
            .post,API_K_BLOG.UPLOAD_BLOG_IMAGE,
            multipartFormData: { multipartFormData in
                if let image = image, let imageData =  image.jpegData(compressionQuality: 0.80) {
                    multipartFormData.append(imageData, withName: "image", fileName: "file.png", mimeType: "application/octet-stream")
                }
                
                /*for (key, value) in params {
                    let val:String = value
                    multipartFormData.append(val.data(using: .utf8)!, withName: key)
                }*/
        },
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON{ (response) -> Void in
                        switch response.result{
                        case .success(let value):
                            let json = JSON(value)
                            print(json)
                            
                            if let jsonDic = json.dictionaryObject {
                                let status:Bool = jsonDic["success"] as! Bool
                                let mssg = jsonDic["message"] as! String
                                let imagedict:[String:String] = jsonDic["data"] as! [String : String]
                                
                                 if let imageName =  imagedict["imageName"] {
                                 completion?(status,imageName,mssg)
                                 }
                                 else{
                                 completion?(status,nil,mssg)
                                 }
                            }
                            else {
                                completion?(false,nil,APP_STRING.SERVER_ERROR)
                            }
                            SVProgressHUD.dismiss()
                            
                        case .failure(let error):
                            SVProgressHUD.dismiss()
                            completion?(false,nil,APP_STRING.SERVER_ERROR)
                        }
                    }
                case .failure(let encodingError):
                    SVProgressHUD.dismiss()
                    completion?(false,nil,APP_STRING.SERVER_ERROR)
                }
        })
    }
    
  
  
}



